<?php

namespace App\Controller;


use Curl\Curl;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/", name="ping")
     * @throws \ErrorException
     */
    public function pingAction()
    {

        $curl = new Curl();
        $curl->get('http://127.0.0.1:8000/?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJvcmdhbml6YXRpb25OYW1lIjoiOlx1MDQyMFx1MDQzZVx1MDQzM1x1MDQzMCBcdTA0MzggXHUwNDNhXHUwNDNlXHUwNDNmXHUwNDRiXHUwNDQyXHUwNDMwIiwib3JnYW5pemF0aW9uU2l0ZVVSTCI6Ijpzb21lLmNvbSIsImlkIjoxfQ.an_JkdnoXfzWRsosc_IX21jv4565do1jTQ3rimGd5do');

        if ($curl->error) {
            return new Response( 'Error: ' . $curl->errorCode . ': ' . $curl->errorMessage . "\n");
        } else {
            return new Response(var_export($curl->response,1));
        }
    }
}
